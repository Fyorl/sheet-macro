Hooks.on('hotbarDrop', (bar, data, slot) => {
	if (data.type === "Actor") {
		onDropActor(bar, data, slot);
		return false;
	}
});

async function onDropActor (bar, data, slot) {
	const actor = await Actor.fromDropData(data);
	if (!actor) {
		return;
	}

	const command = `Hotbar.toggleDocumentSheet('${actor.uuid}')`;

	let macro =
		game.macros.find(macro => macro.name === actor.name && macro.command === command);

	if (!macro) {
		macro = await Macro.create({
			command,
			name: actor.name,
			type: 'script',
			img: actor.img
		}, {renderSheet: false});
	}

	game.user.assignHotbarMacro(macro, slot);
}
